// mailer reference
require('./util/emailer');
// init mysql
// var mysql = require('mysql');

// init express
var express = require('express');
var path = require('path');

// init CORS middleware
var cors = require('cors');

// var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// init routes
var index = require('./routes/index');
var schedule = require('./routes/schedule');
// var users = require('./routes/users');

var app = express();
app.use(cors());
// app.use(cors({credentials: true, origin: true}));
// var connection = mysql.createConnection({
//   host: 'localhost',
//   user: 'jose',
//   password: 'jose',
//   database: 'rentz'
// });

// mongoose ref: http://mongoosejs.com/docs/connections.html#use-mongo-client
// var mongoose = require('mongoose');
// var promise = mongoose.connect('mongodb://localhost:27017/rentz', {
//   useMongoClient: true,
//   /* other options */
// });

// // view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
// app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
//support parsing application/json data
app.use(bodyParser.json());
//support parsing application/x-www-form-urlencoded data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// set routes
app.use('/', index);
app.use('/schedule', schedule);
// app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   // res.header("Access-Control-Allow-Origin", "http://rentz.scorpiosoftwarellc.com/");
//   res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
