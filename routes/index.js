var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var urlEncodedParser = bodyParser.urlencoded({extended: false});

var jsonParser = bodyParser.json();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendfile('./views/index.html');
});

/* GET cath all. */
router.get('*', function(req, res, next) {
  var url = req.url;
  var url = req.url;
  if(url.indexOf('/schedule/') === -1)
      res.status(401).send('Invalid get request!');    
  else
      next();
});
router.get('/*', function(req, res, next) {
  var url = req.url;
  var url = req.url;
  if(url.indexOf('/schedule/') === -1)
      res.status(401).send('Invalid get request 2!');    
  else
      next();
});

/* POST cath all. */
router.post('*', function(req, res, next) {
  var url = req.url;
  if(url.indexOf('/schedule') === -1)
    res.status(401).send('Invalid post 1 request!');    
  else
    next();
});

module.exports = router;
