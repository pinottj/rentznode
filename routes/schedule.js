var express = require('express');
var router = express.Router();
var transporter = require('../util/emailer');
// var Schedule = require('../models/schedule');
// init mysql
var mysql = require('mysql');

/* POST scheduled appointment. */
router.post('/', function(req, res, next) {
    var nowDate = new Date();
    var auth = 'scorpioSoftware'+nowDate.toISOString().slice(0,10).replace(/-/g,"")+'valicsi!';
    var authRequest = new Buffer(req.get('Authorization'), 'base64'); 
    try {
        if(authRequest.toString() === auth) {
            var connection = mysql.createConnection({
                host: 'localhost',
                user: 'jose',
                password: 'jose',
                database: 'rentz'
              });
            connection.connect(function(err) {
                if (err) throw err
                // todo: format date, need to redo it
                scheduledDate = new Date(req.body.date);
                hr = req.body.time.slice(':').slice(0, 2);
                min = req.body.time.slice(':').slice(3, 5);
                scheduledDate.setHours(hr, min);
                var AMPM = req.body.time.slice(':').slice(5, req.body.time.slice(':').length);
                tmp = new Date();
                console.log('You are now connected...');
                connection.query(`INSERT INTO schedule (email, date, time, emailConfirmed, confirmEmailSent, instructionsEmailSent, createdDate) VALUES (?, ?, ?, ?, ?, ?, ?)`, 
                    [req.body.email, scheduledDate, AMPM, 0, 0, 0, new Date()], 
                        function(err, result) {
                            if (err) throw err
                            res.status(201).json({'Status':'Successfully scheduled appointment'});
                            // send the email
                            transporter.mainOptions.html = '<p>Hello,<br /><br />You have chosen to visit our property on ' + scheduledDate + AMPM 
                                    + '.<br /><br />Please click or visit the link: '
                                    + '<a href="http://api.scorpiosoftwarellc.com/schedule/' +result.insertId +'">Confirm Appointment</a>'
                                    + '&nbsp;&nbsp;(if this was in error no action is necessary).'
                                    +'<br /><br />Thank you for your interest in our property. We look forward to serving your housing needs.'
                                    +'<br /><br /><br /><br />Sincerely,<br /><br /><a href="http://rentz.scorpiosoftwarellc.com"/>Rentz</a></p>';
                            transporter.id = result.insertId;
                            transporter.mainOptions.to = req.body.email;
                            callbackFollowUp = function(err, info){
                                if (err) { 
                                    connection.query('UPDATE schedule set confirmEmailSent = -1 where id = '+transporter.id, function(err, results) {
                                        if (err) throw err
                                    });
                                } else {
                                    console.log('confirm email sent!');
                                    connection.query('UPDATE schedule set confirmEmailSent = 1 where id = '+transporter.id, function(err, results) {
                                        if (err) throw err
                                    });
                                }
                            }
                            transporter.callback = callbackFollowUp;
                            // send the email
                            transporter.sendMail(transporter.mainOptions, transporter.callback);          
                });
            });
        } else {
            res.status(401).send('Request Unauthorized!');
        }
    } catch (e) {
        console.log(e);
        res.status(500).send('Error occurred!');
    }
});

/* GET schedules. */
router.get('/*', function(req, res, next) {
    var url = req.url;
    var patt = new RegExp("^[0-9/]+$");
    if(patt.test(url) === false)
        res.status(401).send('Invalid request!');    
    else
        next();
});

/* GET appointment confirmation. */
router.get('/:id', function(req, res, next) {
    try {
        // var id = req.body.id;
        var id = parseInt(req.params.id);
        console.log("val: " + id + ", value: " + !isNaN(id));
        if(id !== '0' && !isNaN(id)) {
            var connection = mysql.createConnection({
                host: 'localhost',
                user: 'jose',
                password: 'jose',
                database: 'rentz'
            });
            connection.connect(function(err) {
                if (err) throw err
                // query appointment
                connection.query('SELECT * FROM schedule where id = '+id+' AND emailConfirmed = 0', function(err, results) {
                    if (err) throw err
                    var nowDate = new Date();
                    if(results.length > 0) { 
                        var scheduleObj = {
                            createdDate:results[0].createdDate,
                            email:results[0].email,
                            date:results[0].date,
                            id: id
                        }
                        // check if link is still good
                        var linkExpiration = scheduleObj.createdDate;
                        linkExpiration.setHours(linkExpiration.getHours() + 1);
                        if(linkExpiration.getTime() > nowDate.getTime()) {
                            connection.query(' UPDATE schedule set emailConfirmed = 1 where id = '+scheduleObj.id, function(err, results) {
                                if (err) throw err
                                // todo: clean this up
                                console.log('Sending property details for id: ' + scheduleObj.id);
                                transporter.mainOptions.to = scheduleObj.email;
                                transporter.mainOptions.subject = 'Rentz - Property Details';
                                transporter.mainOptions.html = '<p>Hello,<br /><br />You have scheduled a house visit on ' + scheduleObj.date + ` and we would like to thank you.
                                        <br /><br />The property address is 5721 Blueridge Dr, Columbus, GA 31907. 
                                        <br /><br />In order to gain access to the property we will need for you to be qualified by <a href="https://www.transunion.com/">TransUnion</a>. You will 
                                        receive a separate email from TransUnion with instructions on how to complete your application. 
                                        <br /><br />Please feel free to contact us rentz@scorpiosoftwarellc.com with any questions or concerns.
                                        <br /><br /><br /><br />Sincerely,<br /><br /><a href="http://rentz.scorpiosoftwarellc.com"/>Rentz</a></p>`;
                                callbackFollowUp = function(err, info){
                                    if (err) { throw err }
                                    console.log('email sent for property details!');
                                    connection.query('UPDATE schedule set instructionsEmailSent = 1 where id = '+scheduleObj.id, function(err, results) {
                                        if (err) throw err
                                    });
                                }
                                transporter.callback = callbackFollowUp;
                                transporter.mainOptions.bcc = 'pinottj@gmail.com';
                                // send the email
                                transporter.sendMail(transporter.mainOptions, transporter.callback);
                                res.sendfile('./views/confirmation.html');
                            });
                        } else
                            res.status(200).json({'Status':'Confirmation link has expired. Please make a new appointment.'});
                    } else {
                        res.status(401).send('Invalid confirmation request!');
                    }
                });
            });
        } else { // this should never happen
            res.status(401).send('Invalid request!');    
        }
    } catch (e) {
        console.log(e);
        res.status(500).send('Error occurred!');
    }
});

module.exports = router;
